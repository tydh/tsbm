# Timescale benchmarking tool

A tool for benchmarking SELECT query performance of a Timescale database
instance.

## Functional requirements

- Read CSV file with parameters for query
- Read flag specifying number of concurrent workers
- Report:
  - number of queries executed
  - total time [^Q1]
  - durations for query execution
    - minimum
    - maximum
    - median
    - average
  - error counts grouped by layer and code [^Q2]

## Non-functional requirements

- Assign query to worker dependent on value of §hostname§ parameter.

## Implementation plan

- Use Golang
- Use https://github.com/lib/pq
- Use https://golang.org/pkg/encoding/csv/

## Open questions

[^Q1]: Should the total time be the accumulated durations of the concurrent processes?

[^Q2]: What is the lowest acceptable level of granularity for errors?
